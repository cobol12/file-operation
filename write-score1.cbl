       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR.NARANYA BOONMEE.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           05 STU-ID         PIC   X(8).
           05 MIDTERM-SCORE  PIC   9(2)V9(2).
           05 FINAL-SCORE    PIC   9(2)V9(2).
           05 PROJECT-SCORE  PIC   9(2)V9(2).

       PROCEDURE DIVISION .
       BEGIN.
           OPEN OUTPUT SCORE-FILE
           MOVE "39030001"   TO STU-ID
           MOVE "24.75"      TO MIDTERM-SCORE
           MOVE "25.17"      TO FINAL-SCORE
           MOVE "80.8"      TO PROJECT-SCORE
           WRITE SCORE-DETAIL 

           MOVE "39030002"   TO STU-ID
           MOVE "25.1"      TO MIDTERM-SCORE
           MOVE "85.26"      TO FINAL-SCORE
           MOVE "10.8"      TO PROJECT-SCORE
           WRITE SCORE-DETAIL 

           MOVE "39030003"   TO STU-ID
           MOVE "88.50"      TO MIDTERM-SCORE
           MOVE "39.14"      TO FINAL-SCORE
           MOVE "75.7"      TO PROJECT-SCORE
           WRITE SCORE-DETAIL 

           MOVE "39030004"   TO STU-ID
           MOVE "60"      TO MIDTERM-SCORE
           MOVE "23"      TO FINAL-SCORE
           MOVE "27"      TO PROJECT-SCORE
           WRITE SCORE-DETAIL 
           CLOSE SCORE-FILE  
           GOBACK 
       .


